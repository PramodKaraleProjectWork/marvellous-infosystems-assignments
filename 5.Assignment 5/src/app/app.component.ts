import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/*

@Component({
  selector: 'app-root',
  template: `<table>
  <tr>
    <td>
      <label class="Labels">Enter First Name:</label>  
    </td>
    <td>
<input class="TextInput" type="text" />
    </td>
  </tr>
</table>`,
  styles: [`.Labels
  {
      color:blue;
  }
  
  
  .TextInput
  {
      border-color:black ;
  }`]
})
*/
export class AppComponent {
  title = 'Assignment';
}
